#ifndef FRMTEST_HPP
#define FRMTEST_HPP

#include <gtkmm.h>
#include "logoi.hpp"
#include "frmFinished.hpp"

class frmTest
{
	public:
		frmTest(Glib::RefPtr<Gtk::Builder> builder);
		~frmTest();
		
		Gtk::Window* window;
		Gtk::Label* lblQuestion;
		Gtk::Entry* txtAnswer;
		Gtk::Button* cmdOpenCancel;
		Gtk::Button* cmdNextFinish;

	protected:
		/* Start the test. list must be ready before this function is called. */
		void start_test();

		/* Finish the test. */
		void finish_test();

		/* Finish dialog */
		frmFinished finish_dialog;

		/* Wordlist */
		logoi::wrtslist list;
		/* Iterator for wordlist */
		std::vector<logoi::wordpair>::iterator i;

		/* Current filename */
		std::string current_filename;

		/* Points */
		int right_answers = 0;
		int wrong_answers = 0;

		/* True if testing right now */
		bool testing = false;

		/* Signal handlers */
		void cmdOpenCancel_click();
		void cmdNextFinish_click();

		/* Some UI constants. */
		const std::string str_next = "Next";
		const std::string str_finish = "Finish";
		const std::string str_retry = "Retry";
		const std::string str_open = "Open";
		const std::string str_cancel = "Cancel";
};

#endif /* FRMTEST_HPP */

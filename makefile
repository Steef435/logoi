CXX ?= g++
RM ?= rm -f

LDFLAGS= `pkg-config --libs gtkmm-3.0`
CPPFLAGS = -std=c++11 `pkg-config --cflags gtkmm-3.0`
CFDEBUG = -Wall

EXEC = logoi
SRCS = testlist.cpp wrtslist.cpp frmTest.cpp highscores.cpp highscore_entry.cpp frmFinished.cpp main.cpp
OBJS = ${SRCS:.cpp=.o}

PREFIX?=/usr
BINDIR=${PREFIX}/bin
DATADIR=${PREFIX}/share/${EXEC}

all: ${EXEC}

${EXEC}: ${OBJS}
	${CXX} ${CPPFLAGS} -o ${EXEC} ${OBJS} ${LDFLAGS}

debug: ${EXEC}
debug: CXX += ${CFDEBUG}

clean:
	${RM} ${OBJS}

install: ${EXEC}
	install -D -m 755 ${EXEC} ${DESTDIR}${BINDIR}/${EXEC}
	install -D -m 755 logoi.glade ${DESTDIR}${DATADIR}/logoi.glade

uninstall:
	rm -r ${DESTDIR}${BINDIR}/${EXEC} ${DESTDIR}${DATADIR}/logoi.glade
	rmdir --ignore-fail-on-non-empty ${DESTDIR}${DATADIR}

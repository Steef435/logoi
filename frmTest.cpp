#include "frmTest.hpp"
#include <ctime>

frmTest::frmTest(Glib::RefPtr<Gtk::Builder> builder)
	:finish_dialog(builder)
{
	/* Fetch objects  */
	builder->get_widget("frmTest", window);
	builder->get_widget("lblQuestion", lblQuestion);
	builder->get_widget("txtAnswer", txtAnswer);
	builder->get_widget("cmdOpenCancel", cmdOpenCancel);
	builder->get_widget("cmdNextFinish", cmdNextFinish);

	/* Connect signals */
	cmdOpenCancel->signal_clicked().connect(sigc::mem_fun(*this, &frmTest::cmdOpenCancel_click));
	cmdNextFinish->signal_clicked().connect(sigc::mem_fun(*this, &frmTest::cmdNextFinish_click));

	/* Make finish_dialog transient */
	finish_dialog.window->set_transient_for(*window);
}

frmTest::~frmTest()
{
	/* Release objecst */
	delete window;
	delete lblQuestion;
	delete txtAnswer;
	delete cmdOpenCancel;
	delete cmdNextFinish;
}

void frmTest::start_test()
{
	/* Start testing! */
	testing = true;

	/* Reset score */
	right_answers = 0;

	/* Shuffle the list */
	list.shuffle();

	/* Set iterator to begin of words */
	i = list.words.begin();

	/* Set the labels of the buttons */
	cmdOpenCancel->set_label(str_cancel);
	if (i < (list.words.end() - 1))
		cmdNextFinish->set_label(str_next);
	else
		cmdNextFinish->set_label(str_finish);

	/* Set the window title */
	window->set_title(list.get_title() + " - Logoi");

	/* Ask the question */
	lblQuestion->set_text(i->first);

	/* Show the next button if it's invisible */
	cmdNextFinish->show();
}

void frmTest::finish_test()
{
	/* Stop testing! */
	testing = false;

	/* Highscores etc. */
	finish_dialog.prepare(right_answers, int(list.words.size() - right_answers), list.get_title(), time(NULL), current_filename);
	finish_dialog.window->run();

	/* Set the buttons */
	cmdOpenCancel->set_label(str_open);
	cmdNextFinish->set_label(str_retry);

	/* Reset label */
	lblQuestion->set_text("");
}

void frmTest::cmdOpenCancel_click()
{
	/* If not testing, open file */
	if (testing)
	{
		/* Cancel test */
		testing = false;

		/* Reset iterator */
		i = list.words.begin();

		/* Reset question and answer */
		lblQuestion->set_text("");
		txtAnswer->set_text("");

		/* Set buttons */
		cmdNextFinish->set_label(str_retry);
		cmdOpenCancel->set_label(str_open);
	}
	else
	{
		/* Open file */
		Gtk::FileChooserDialog filedialog("Choose wrts file", Gtk::FILE_CHOOSER_ACTION_OPEN);
		filedialog.set_transient_for(*window);
		filedialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
		filedialog.add_button("Select", Gtk::RESPONSE_OK);

		/* Show the dialog and if selected parse the file */
		if (filedialog.run() == Gtk::RESPONSE_OK)
		{
			list.readfile(filedialog.get_filename());

			/* Store the filename for later use */
			current_filename = filedialog.get_filename();

			/* Start the test! */
			start_test();
		}
	}
}

void frmTest::cmdNextFinish_click()
{
	if (testing)
	{
		/* Check if the answer is correct */
		if (txtAnswer->get_text() == i->second)
		{
			/* Right answer! */
			right_answers++;
		}
		else
		{
			/* Wrong answer! */
			wrong_answers++;
		}

		/* Check if this is the last question */
		if (i == list.words.end() - 1)
		{
			/* This is/was the last question. Time to finish the test. */
			finish_test();
		}
		else
		{
			/* This isn't the last question. Let's go to the next one! */
			i++;
			lblQuestion->set_text(i->first);
			txtAnswer->set_text("");

			/* Is the new question the last? */
			if (i == list.words.end() - 1)
			{
				/* It is. Let's show that. */
				cmdNextFinish->set_label(str_finish);
			}
		}
	}
	else
	{
		/* Retry */
		start_test();
	}
}

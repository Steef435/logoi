/* Logoi, a wordlist interpreter library
 * Written by Steef Hegeman for the logoi project
 * The author(s) is/are not responsible for any damage. Use at your own risk.
 * Kittens can and will be separated from their homes. You have been warned.
 * 
 * Currently, there is built-in support for the following lists:
 * wrts (www.wrts.nl)
 * It's easy to add support for more list types. See "testlist.hpp" for more details */

#ifndef LOGOI_HPP
#define LOGOI_HPP

#include "word.hpp"
#include "testlist.hpp"
#include "wrtslist.hpp"
#include "highscore_entry.hpp"
#include "highscores.hpp"

#endif /* LOGOI_HPP */

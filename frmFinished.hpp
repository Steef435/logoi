#ifndef FRMFINISHED_HPP
#define FRMFINISHED_HPP

#include <gtkmm.h>
#include "highscores.hpp"
#include "highscore_entry.hpp"

class frmFinished
{
	public:
		/* Fetch widgets from builder, connect signals etc */
		frmFinished(Glib::RefPtr<Gtk::Builder> builder);
		/* Free widgets */
		~frmFinished();

		/* Prepare for show().
		 * right - Right answers
		 * wrong - Wrong answers
		 * title - Title of list
		 * time - Time of completion(in seconds since epoch)
		 * filename - filename of list(for highscores) */
		void prepare(const unsigned int& right, const unsigned int& wrong, const std::string& title, const time_t& time, const std::string& filename);

		/* Pointers to some widgets(the widgets are managed by builder) */
		Gtk::Dialog* window;
		Gtk::Label* lblRight;
		Gtk::Label* lblWrong;
		Gtk::Label* lblTotal;
		Gtk::TreeView* treeHighscore;
		Gtk::Entry* txtName;
		Gtk::Button* cmdAdd;
		Gtk::Button* cmdClose;

	protected:
		/* Highscores */
		logoi::highscores scores;

		/* Path to highscores data */
		const std::string highscores_filename = Glib::build_filename(Glib::get_user_data_dir(), Glib::get_prgname(), "highscores.xml");

		/* Current filename */
		std::string current_filename;

		/* Current entry(without name) */
		logoi::highscore_entry current_entry;

		/* treeview columns */
		class highscoreColumns : public Gtk::TreeModel::ColumnRecord
		{
			public:
				highscoreColumns()
				{ add(name); add(score); add(time); }

				/* Name */
				Gtk::TreeModelColumn<std::string> name;
				/* Score */
				Gtk::TreeModelColumn<unsigned int> score;
				/* Time in readable format(std::asctime(std::localtime(time))) */
				Gtk::TreeModelColumn<std::string> time;
		};

		/* liststore(data for treeview) */
		Glib::RefPtr<Gtk::ListStore> listStore;

		/* Columns */
		highscoreColumns columns;

		/* Show highscores for file filename in the treeview */
		void show_scores(const std::string& filename);

		/* Signal handlers */
		/* Create entry and add it to highscore */
		void cmdAdd_click();

		/* Close window */
		void cmdClose_click();
};

#endif /* FRMFINISHED_HPP */

/* Implementation of the class testlist. Refer to testlist.hpp for more details.
 * This is part of the logoi project. See logoi.hpp for more details */

#include "testlist.hpp"
#include <random>
#include <algorithm>

namespace logoi
{
	const std::string testlist::get_title() const
	{
		/* Return the title of the list */
		return title;
	}

	const std::pair<std::string, std::string> testlist::get_languages() const
	{
		/* Return the languages of the list */
		return languages;
	}

	void testlist::shuffle()
	{
		/* Create random number generator */
		std::random_device rd;
		std::mt19937 g(rd());
		/* Shuffle the words with random number generator */
		std::shuffle(words.begin(), words.end(), g);
	}
}

/* This is part of the logoi project. See logoi.hpp for more details */

#ifndef WRTSLIST_HPP
#define WRTSLIST_HPP

#include "testlist.hpp"

namespace logoi
{
	/* Reads and stores a list of words and some other properties of a wrts list file
	 * Derived from base class testlist. See testlist.hpp for more details */
	class wrtslist : public testlist
	{
		public:
			/* Read the contents of the wrts list file filename and store them */
			void readfile(std::string filename);
	};
}

#endif /* WRTSLIST_HPP */

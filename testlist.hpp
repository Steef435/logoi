/* This is part of the logoi project. See logoi.hpp for more details */

#ifndef TESTLIST_HPP
#define TESTLIST_HPP

#include <string>
#include "word.hpp"
#include <vector>

namespace logoi
{
	/* (Base) class of object storing the list of words and some other properties of a list file
	 * Functions that *must* be overriden:
	 * virtual void readfile(std::string filename)
	 * See wrtslist.hpp and wrtslist.cpp for an example */
	class testlist
	{
		public:
			/* Returns the title of the list */
			const std::string get_title() const;

			/* Returns the languages of the list */
			const std::pair<std::string, std::string> get_languages() const;

			/* Read file filename. */
			virtual void readfile(std::string filename) =0;

			/* Shuffle the list of wordpairs (words) */
			virtual void shuffle();

			/* List of wordpairs. */
			std::vector<wordpair> words;

		protected:
			/* Title of the list */
			std::string title;

			/* Languages of the list */
			std::pair<std::string, std::string> languages;
	};
}

#endif /* TESTLIST_HPP */

#include "highscores.hpp"
#include "rapidxml.hpp"
#include "rapidxml_print.hpp"
#include <glibmm.h>
#include <fstream>
#include <sstream>
#include "util.hpp"

using namespace rapidxml;

namespace logoi
{
	/* Parse highscores file */
	void highscores::read(const std::string& filename)
	{
		xml_document<> doc;
		
		/* Read the file into zero-terminated vector<char> */
		std::ifstream finput(filename);

		/* Some error handling */
		if (!finput)
			throw std::ios::failure("Error opening file");

		std::vector<char> buffer((std::istreambuf_iterator<char>(finput)), std::istreambuf_iterator<char>());
		finput.close();
		buffer.push_back('\0');

		/* Parse it */
		doc.parse<0>(&buffer[0]);

		/* Find the root node */
		xml_node<>* root = doc.first_node("highscores");
		if (root)
		{
			/* Loop through all files */
			xml_node<>* filenode = root->first_node("file");
			while (filenode)
			{
				/* Parse file */
				entrylist.emplace(std::make_pair(filenode->first_attribute("checksum")->value(), std::list<highscore_entry>()));

				/*Loop through all entries */
				xml_node<>* entrynode = filenode->first_node("entry");
				while (entrynode)
				{
					/* Parse entry */
					/* Convert char* score to unsigned int */
					std::stringstream ss(entrynode->first_node("score")->value());
					unsigned int score;
					ss >> score;

					/* Convert char* time to time_t */
					std::stringstream ss2(entrynode->first_node("time")->value());
					time_t time;
					ss2 >> time;

					entrylist.at(filenode->first_attribute("checksum")->value()).emplace_back(entrynode->first_node("name")->value(), score, time );

					/* Next entry */
					entrynode = entrynode->next_sibling("entry");
				}

				/* Order the entries */
				entrylist.at(filenode->first_attribute("checksum")->value()).sort(std::greater<highscore_entry>());

				/* Next filenode */
				filenode = filenode->next_sibling("file");
			}
		}
	}

	const std::list<highscore_entry> highscores::get_highscores(const std::string& filename)
	{
		/* First, generate a checksum of the file so we can recognize it */
		std::string sum = Glib::Checksum::compute_checksum(Glib::Checksum::ChecksumType::CHECKSUM_SHA256, Glib::file_get_contents(filename));

		/* Return const reference to list for file filename, if it is registered */
		if (entrylist.find(sum) != entrylist.end())
			return entrylist.at(sum);

		/* The file isn't registered, return an empty list */
		return std::list<highscore_entry>();
	}

	bool highscores::check_score(const std::string& filename, const highscore_entry& entry) const
	{
		/* First, generate a checksum of the file so we can recognize it */
		std::string sum = Glib::Checksum::compute_checksum(Glib::Checksum::ChecksumType::CHECKSUM_SHA256, Glib::file_get_contents(filename));

		/* If the file isn't already registered, there is place */
		if (entrylist.find(sum) == entrylist.end())
			return true;

		/* The file is registered. If there are still open spots there is place */
		if (entrylist.at(sum).size() < 5)
			return true;

		/* If entry > the lowest of the current scores, there is place */
		if (entry > entrylist.at(sum).back())
		{
			/* There is place */
			return true;
		}

		/* There is no place */
		return false;
	}

	bool highscores::add_score(const std::string& filename, const highscore_entry& entry)
	{
		/* Check if there is place */
		if (check_score(filename, entry))
		{
			/* First, generate a checksum of the file so we can recognize it */
			std::string sum = Glib::Checksum::compute_checksum(Glib::Checksum::ChecksumType::CHECKSUM_SHA256, Glib::file_get_contents(filename));

			/* If the file isn't already registered, register it */
			if (entrylist.find(sum) == entrylist.end())
			{
				entrylist.emplace(std::make_pair(sum, std::list<highscore_entry>()));
			}

			/* Add the entry */
			entrylist.at(sum).push_back(entry);
			/* Sort the list */
			entrylist.at(sum).sort(std::greater<highscore_entry>());

			/* If there are more than 5 elements, remove the lowest(first) */
			if (entrylist.at(sum).size() > 5)
				entrylist.at(sum).pop_back();			

			/* The entry has been placed */
			return true;
		}

		/* The entry hasn't been placed */
		return false;
	}

	/* Write highscores data to file filename */
	void highscores::write(const std::string& filename)
	{
		xml_document<> doc;

		/* Root node */
		xml_node<>* root = doc.allocate_node(node_element, doc.allocate_string("highscores"));
		doc.append_node(root);

		/* Loop through files */
		for (std::pair<std::string, std::list<highscore_entry> > file : entrylist)
		{
			/* Create file element */
			xml_node<>* filenode = doc.allocate_node(node_element, doc.allocate_string("file"));
			/* Add checksum */
			filenode->append_attribute(doc.allocate_attribute(doc.allocate_string("checksum"), doc.allocate_string(file.first.c_str())));
			/* Add node to root */
			root->append_node(filenode);

			/* Loop through entries */
			for (highscore_entry entry : file.second)
			{
				/* Create entry element */
				xml_node<>* entrynode = doc.allocate_node(node_element, doc.allocate_string("entry"));
				filenode->append_node(entrynode);
					
				/* Add subelements */
				xml_node<>* namenode = doc.allocate_node(node_element, doc.allocate_string("name"), doc.allocate_string(entry.name.c_str()));
				entrynode->append_node(namenode);

				xml_node<>* scorenode = doc.allocate_node(node_element, doc.allocate_string("score"), doc.allocate_string(to_str<unsigned int>(entry.score).c_str()));
				entrynode->append_node(scorenode);

				xml_node<>* timenode = doc.allocate_node(node_element, doc.allocate_string("time"), doc.allocate_string(to_str<time_t>(entry.time).c_str()));
				entrynode->append_node(timenode);
			}
		}

		/* Now the data is in order, write it to a file */
		std::ofstream ofile(filename.c_str());
		ofile<<doc;
		ofile.close();
	}
}

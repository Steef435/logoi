#include "frmFinished.hpp"
#include <ctime>
#include "util.hpp"
#include <iostream>
#include <glibmm.h>
#include <giomm.h>

frmFinished::frmFinished(Glib::RefPtr<Gtk::Builder> builder)
{
	/* Fetch widgets */
	builder->get_widget("frmFinished", window);
	builder->get_widget("lblRight", lblRight);
	builder->get_widget("lblWrong", lblWrong);
	builder->get_widget("lblTotal", lblTotal);
	builder->get_widget("treeHighscore", treeHighscore);
	builder->get_widget("txtName", txtName);
	builder->get_widget("cmdAdd", cmdAdd);
	builder->get_widget("cmdClose", cmdClose);

	/* Connect signals */
	cmdAdd->signal_clicked().connect(sigc::mem_fun(*this, &frmFinished::cmdAdd_click));
	cmdClose->signal_clicked().connect(sigc::mem_fun(*this, &frmFinished::cmdClose_click));

	/* Create liststore with columns */
	listStore = Gtk::ListStore::create(columns);
	/* Let treeHighscore show listStore */
	treeHighscore->set_model(listStore);

	/* Add treeHighscore columns */
	treeHighscore->append_column("Name", columns.name);
	treeHighscore->append_column("Score", columns.score);
	treeHighscore->append_column("Date", columns.time);

	/* Load highscores */
	try
	{
		scores.read(highscores_filename);
	}
	catch (const std::ios::failure& ex)
	{
		std::cerr<<"Non-fatal highscores error: "<<ex.what()<<std::endl;
	}
}

frmFinished::~frmFinished()
{
	delete window;
	delete lblRight;
	delete lblWrong;
	delete lblTotal;
	delete treeHighscore;
	delete txtName;
	delete cmdAdd;
	delete cmdClose;
}

void frmFinished::prepare(const unsigned int& right, const unsigned int& wrong, const std::string& title, const time_t& time, const std::string& filename)
{
	/* Set labels */
	lblRight->set_text(to_str<unsigned int>(right));
	lblWrong->set_text(to_str<unsigned int>(wrong));
	lblTotal->set_text(to_str<unsigned int>(right + wrong));

	/* Set title */
	window->set_title("Finished " + title + " - Logoi");

	/* Highscores */
	show_scores(filename);

	/* Check if a position on the highscore list is available */
	if (scores.check_score(filename, logoi::highscore_entry("", right, time)))
	{
		/* Position available! */
		cmdAdd->show();
		txtName->show();
		txtName->set_text(Glib::get_real_name());
	}
	else
	{
		/* No position available */
		cmdAdd->hide();
		txtName->hide();
	}

	/* Save filename and entry for use in cmdAdd_click() */
	current_filename = filename;
	current_entry.name = "";
	current_entry.score = right;
	current_entry.time = time;
}

void frmFinished::show_scores(const std::string& filename)
{
	/* Clear the list */
	listStore->clear();
	/* Loop through the highscores */
	for (logoi::highscore_entry i : scores.get_highscores(filename))
	{
		/* Place info in treeview */
		/* Create row */
		Gtk::TreeModel::Row row = *(listStore->append());
		/* Fill row */
		row[columns.name] = i.name;
		row[columns.score] = i.score;
		row[columns.time] = std::asctime(std::localtime(&i.time));
	}
}

void frmFinished::cmdAdd_click()
{
	/* Add name to current_entry and add it to the highscores */
	current_entry.name = txtName->get_text();
	scores.add_score(current_filename, current_entry);
	/* Show new highscores */
	show_scores(current_filename);

	/* Create directory if it doesn't exist */
	if (!Glib::file_test(Glib::path_get_dirname(highscores_filename), Glib::FILE_TEST_EXISTS))
	{
		/* Create directory and parents */
		Gio::File::create_for_path(Glib::path_get_dirname(highscores_filename))->make_directory_with_parents();
	}

	/* Write new highscores to file */
	scores.write(highscores_filename);

	/* Hide */
	cmdAdd->hide();
	txtName->hide();
}

void frmFinished::cmdClose_click()
{
	window->hide();
}

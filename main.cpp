#include <gtkmm.h>
#include <glibmm.h>
#include <giomm.h>
#include "logoi.hpp"
#include <iostream>
#include "frmTest.hpp"
#include <vector>
#include <string>

int main(int argc, char** argv)
{
	/* Create app */
	Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "org.logoi");
	Glib::set_prgname("logoi");
	Glib::set_application_name("logoi");

	/* Look for glade file(contains gui objects) */
	/* Get data directories */
	std::vector<std::string> dirs = Glib::get_system_data_dirs();

	for (std::vector<std::string>::iterator i = dirs.begin(); i != dirs.end(); ++i)
	{
		/* Add "logoi" to path */
		*i = Glib::build_filename(*i, "logoi");
	}

	dirs.push_back("."); /* Add current directory as last resort */

	std::vector<std::string>::iterator i = dirs.begin();
	/* As long as we can't find the logoi.glade file within the current dir */
	while (!Glib::file_test(Glib::build_filename(*i, "logoi.glade"), Glib::FILE_TEST_EXISTS))
	{
		/* Go to the next dir */
		i++;

		/* If we have checked all directories and haven't found the file, panic */
		if (i == dirs.end())
		{
			std::cerr<<"Fatal: Can't find logoi.glade file in data directories or current directory!"<<std::endl;
			return 1;
		}
	}

	/* Load glade file(contains gui objects) */
	Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create();
	try
	{
		builder->add_from_file(Glib::build_filename(*i, "logoi.glade"));
	}
	catch(const Glib::FileError& ex)
	{
		std::cerr << "FileError: " << ex.what() << std::endl;
		return 1;
	}
	catch(const Glib::MarkupError& ex)
	{
		std::cerr << "MarkupError: " << ex.what() << std::endl;
		return 1;
	}
	catch(const Gtk::BuilderError& ex)
	{
		std::cerr << "BuilderError: " << ex.what() << std::endl;
		return 1;
	}

	/* Initialize gui */
	frmTest test(builder);

	/* Run the app */
	return app->run(*test.window);
}

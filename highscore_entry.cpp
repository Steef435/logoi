#include "highscore_entry.hpp"

namespace logoi
{
	highscore_entry::highscore_entry(){}

	highscore_entry::highscore_entry(const std::string& _name, const unsigned int& _score, const std::time_t& _time)
		:name(_name), score(_score), time(_time){}

	bool operator<(const highscore_entry& lhs, const highscore_entry& rhs)
	{
		/* The score is the most important aspect. If lhs.score is less than lhs is less */
		if (lhs.score < rhs.score)
			return true;
		else if (lhs.score == rhs.score)
		{
			/* If the scores are the same: if lhs.time > rhs.time then lhs < rhs*/
			if (lhs.time > rhs.time)
				return true;
		}
		/* lhs >= rhs */
		return false;
	}
}

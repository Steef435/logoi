/* This is part of the logoi project. See logoi.hpp for more details */

#ifndef HIGHSCORE_ENTRY_HPP
#define HIGHSCORE_ENTRY_HPP

#include <string>
#include <ctime>

namespace logoi
{
	/* Class to store highscore attempt/entry */
	class highscore_entry
	{
		public:
			highscore_entry();
			highscore_entry(const std::string& _name, const unsigned int& _score, const std::time_t& _time);

			/* Submitter's name */
			std::string name;

			/* Submitter's score */
			unsigned int score;

			/* Time of finish test */
			std::time_t time;
	};

	/* Comparison operators */
	bool operator<(const highscore_entry& lhs, const highscore_entry& rhs);
	inline bool operator>(const highscore_entry& lhs, const highscore_entry& rhs){return  operator< (rhs,lhs);}
}

#endif /* HIGHSCORE_ENTRY_HPP */

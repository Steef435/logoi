/* This is part of the logoi project. See logoi.hpp for more details */

#ifndef WORD_HPP
#define WORD_HPP

namespace logoi
{
	/* Alias for a pair of words */
	using wordpair = std::pair<std::string, std::string>; /* NOTE: Alias requires C++11 or higher */
}

#endif /* WORD_HPP */

#ifndef UTIL_HPP
#define UTIL_HPP

#include <sstream>

/* Convert T to string */
template <class Ti>
const std::string to_str(const Ti& i)
{
	std::stringstream ss;
	ss << i;
	return ss.str();
}

/* Convert std::string to To*/
template <class To>
const To str_to(const std::string& i)
{
	std::stringstream ss(i);
	To o;
	ss >> o;
	return o;
}

#endif /* UTIL_HPP */

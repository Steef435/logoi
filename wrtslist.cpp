/* Implementation of the class wrtslist. Refer to wrtslist.hpp for more details
 * This is part of the logoi project. See logoi.hpp for more details */

#include "wrtslist.hpp"
#include <fstream>
#include "rapidxml.hpp"

using namespace rapidxml;

namespace logoi
{
	/* Parse wrts file filename */
	void wrtslist::readfile(std::string filename)
	{
		xml_document<> doc;

		/* Read the file into zero-terminated vector<char> */
		std::ifstream finput(filename);

		/* Some error handling */
		if (!finput)
			throw std::ios::failure("Error opening file");


		std::vector<char> buffer((std::istreambuf_iterator<char>(finput)), std::istreambuf_iterator<char>());
		finput.close();
		buffer.push_back('\0');

		/* Parse it */
		doc.parse<0>(&buffer[0]);

		xml_node<>* listnode = doc.first_node("wrts")->first_node("lijst");

		/* Get some general properties */
		/* Title */
		title = listnode->first_node("titel")->value();
		/* Languages */
		languages = std::pair<std::string, std::string>(listnode->first_node("taal")->first_node("a")->value(), listnode->first_node("taal")->first_node("b")->value());

		/* Extract the list */
		xml_node<>* pair = doc.first_node("wrts")->first_node("lijst")->first_node("woord");

		/* Clear words */
		words.clear();

		/* Cycle through all words */
		while (pair)
		{
			/* Add word to the list */
			words.push_back(wordpair(pair->first_node("a")->value(), pair->first_node("b")->value()));
			pair = pair->next_sibling("woord");
		}
	}
}

/* This is part of the logoi project. See logoi.hpp for more details */

#ifndef HIGHSCORES_HPP
#define HIGHSCORES_HPP

#include "highscore_entry.hpp"
#include <unordered_map>
#include <list>
#include <vector>

/* highscores
 * A class for reading and managing the highscore of a file
 * Highscores are separated per file. A file is recognized by its checksum.
 * All highscores for all files are saved in one file.
 *
 * For example, a highscores file could look like this:
 *
 * <highscores>
 * 	<file checksum="13550350a8681c84c861aac2e5b440161c2b33a3e4f302ac680ca5b686de48de">
 * 		<entry>
 * 			<name>John</name>
 * 			<score>7</score>
 * 			<time>234324</time>
 * 		</entry>
 * 	</file>
 * 	<file checksum="30522c770a70cb91f274c82594af301a1f2be8e7f4446e06855533140cdffd27">
 * 		<entry>
 * 			<name>Mary</name>
 * 			<score>5</score>
 * 			<time>1404582088</time>
 * 		</entry>
 * 		<entry>
 * 			<name>Peter</name>
 * 			<score>3</score>
 * 			<time>1304582088</time>
 * 		</entry>
 * 	</file>
 * </highscore>
 *
 * TL;DR: One file. Highscores per testfile. Testfiles identified by checksum. */

namespace logoi
{
	class highscores
	{
		public:
			/* Read highscores from a file. */
			void read(const std::string& filename = "highscore.xml");

			/* Get highscores for file filename */
			const std::list<highscore_entry> get_highscores(const std::string& filename);

			/* Check if entry entry would get in the highscores if it were to be submitted.
			 * True - Yes it would
			 * False - No it wouldn't */
			bool check_score(const std::string& filename, const highscore_entry& entry) const;

			/* Add a highscore entry for a testfile.
			 * filename: filename of the testfile
			 * entry: highscore_entry of the attempt
			 * Return value:
			 * True - The entry made it to the highscores 
			 * False - The entry didn't make it to the highscores */
			bool add_score(const std::string& filename, const highscore_entry& entry);

			/* Write the (half-readable) highscores to a file.
			 * Note that this is *not* intended to be read by users */
			void write(const std::string& filename = "highscore.xml");

		protected:
			std::unordered_map<std::string, std::list<highscore_entry> > entrylist;
	};
}

#endif /* HIGHSCORES_HPP */
